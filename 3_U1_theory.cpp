//U(1) gauge theory
//Debug flags: -DDEBUG_MODE_TERMINAL, -DDEBUG_MODE_LOGFILE, -DFIXED_SEED

#include <vector>
#include <queue>
#include <utility>
#include <iostream>
#include <iterator>
#include <iomanip>
#include <fstream>
#include <filesystem>
#include <string>
#include <ctime>
#include <chrono>
#include <random>
#include <cmath>
#include <complex>
#include <algorithm>
#include <omp.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

//-----

std::string program_version = "U(1)_Gauge_Theory_1.1";

//-----

int N_s;																// Size along s dimension
int N_t;																// Size along t dimension
double LatticeSizeInverse;												// Inverse Lattice size
std::string N_sString;													// Size along s dimension (string)
std::string N_tString;													// Size along t dimension (string)
std::string LatticeSizeInverseString;									// Inverse Lattice size (string)
double mass;															// mass
double beta;															// coupling beta
int n_run;																// Number of runs
int n_count {0};														// Current run
int expectation_period;													// Number of updates between calculation of expectation values
int n_expectation_values;												// Number of expectation values
int append;																// Directory name appendix
std::string appendString;												// Directory name Appendix (string)
std::string directoryname_pre;											// Directory name (prefix)
std::string directoryname;												// Directory name
std::string logfilepath;												// Filepath (log)
std::string positionfilepath;											// Filepath (position)
std::string correlationfilepath;										// Filepath (correlation function)
std::string configfilepath;												// Filepath (config)
std::string finalconfigfilepath;										// File path (final config)
auto start = std::chrono::system_clock::now();							// Start time
std::time_t start_time;													// Start time
double beta_increments_distance;										// Distance between each beta increment
std::vector<std::vector<std::vector<double>>> GaugeField;				// Vector containing the gauge field (x, y, direction)
std::vector<std::vector<double>> GaugeTransformation;					// Vector containing gauge transformations
std::random_device randomdevice;										// Creates random device to seed PRNGs
std::ofstream datalog;													// Output stream to save data
const std::complex<double> i(0, 1);										// Imaginary unit
std::complex<double> staple_1;											// First part of staple
std::complex<double> staple_2;											// Second part of staple
std::complex<double> staple_sum;										// Sum of staple parts
double TopCharge {0};													// Topological charge
double TopNormalization {1/(2*M_PI)};									// Normalization factor for topological charge

//-----

using std::cin;
using std::cout;
using std::endl;
using std::to_string;

//-----

void Configuration()
{
	cout << "\n" << "Please enter beta: ";
	cin >> beta;
	cout << "\n" << "Please enter N_s: ";
	cin >> N_s;
	cout << "\n" << "Please enter N_t: ";
	cin >> N_t;
	cout << "\n" << "Please enter number of runs n_run: ";
	cin >> n_run;
	cout << "\n" << "Please enter expectation_period: ";
	cin >> expectation_period;
	LatticeSizeInverse = 1/double(N_s * N_t);
	cout << "\n" << "Inverse Lattice size is " << LatticeSizeInverse << "\n";
	n_expectation_values = int(n_run/expectation_period);
	cout << "\n" << "n_expectation_values is " << n_expectation_values << "\n";
	//cout << "\n" << "Beta ist " << beta << ".\n";
	//cout << "Die gewählte Gittergröße ist " << PathSize << ", n_run ist " << n_run << " und expectation_period ist " << expectation_period << ".\n";
	//cout << "PathSizeHalf_d ist " << PathSizeHalf_d << " und PathSizeHalf ist " << PathSizeHalf << ".\n";
}

//-----

void SaveParameters(std::string filename, const std::string& starttimestring)
{
	datalog.open(filename, std::fstream::out | std::fstream::app);
	datalog << program_version << "\n";
	datalog << "logfile\n\n";
	#ifdef DEBUG_MODE_TERMINAL
	datalog << "DEBUG_MODE_TERMINAL\n";
	#endif
	datalog << starttimestring << "\n";
	datalog << "N_s = " << N_s << "\n";
	datalog << "N_t = " << N_t << "\n";
	datalog << "beta = " << beta << "\n";
	datalog << "n_run = " << n_run << "\n";
	datalog << "expectation_period = " << expectation_period << "\n";
	datalog.close();
}

//-----

void CreateFiles()
{
	N_sString = to_string(N_s);
	N_tString = to_string(N_t);
	directoryname_pre = "U1_N_s=" + N_sString + "N_t=" + N_tString;
	directoryname = directoryname_pre;
	append = 1;

	while (std::filesystem::exists(directoryname) == true)
	{
		appendString = to_string(append);
		directoryname = directoryname_pre + " (" + appendString + ")";
		++append;
	}

	std::filesystem::create_directory(directoryname);
	cout << "\n\n" << "Verzeichnis \"" << directoryname << "\" erstellt.\n";
	logfilepath = directoryname + "/log.txt";
	configfilepath = directoryname + "/first_config.txt";
	finalconfigfilepath = directoryname + "/final_config.txt";
	cout << "Dateipfad (log): \t" << logfilepath << "\n";
	cout << "Dateipfad (config): \t" << configfilepath << "\n";
	cout << "Dateipfad (finalconfig): \t" << finalconfigfilepath << "\n\n";
	#ifdef DEBUG_MODE_TERMINAL
	cout << "DEBUG_MODE_TERMINAL\n\n";
	#endif	

	//-----
	//Writes parameters to files

	std::time_t start_time = std::chrono::system_clock::to_time_t(start);
	std::string start_time_string = std::ctime(&start_time);

	//logfile

	SaveParameters(logfilepath, start_time_string);

	//configfile

	//SaveParameters(configfilepath, start_time_string);

	//final configfile

	//SaveParameters(finalconfigfilepath, start_time_string);
}

//-----

void ColdStart(std::vector<std::vector<std::vector<double>>>& GaugeField)
{
	/*GaugeField.resize(PathSize);
	std::fill(GaugeField.begin(), GaugeField.end(), 0);

	for (int a = 0; a < PathSize; ++a)
	{
		cout << "GaugeField = " << GaugeField[a] << "\t (ColdStart)\n";
	}*/

	//GaugeField.reserve(LatticeSize);
	//GaugeField.clear();

	//Creates vector containing spins

	#pragma omp parallel for //private(b)
	for (int a = 0; a < N_s; ++a)
	{
		std::vector<std::vector<double>> GaugeFieldRow;
		//GaugeFieldRow.reserve(N_t);
		for (int b = 0; b < N_t; ++b)
		{
			std::vector<double> GaugeFieldDirection;
			GaugeFieldDirection.reserve(2);
			for (int c = 0; c < 2; ++c)
			{
				GaugeFieldDirection.push_back(0.0);
			}
			GaugeFieldRow.push_back(GaugeFieldDirection);
		}
		GaugeField.push_back(GaugeFieldRow);
	}

	/*for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			for (int c = 0; c < 2; ++c)
			{
				cout << "GaugeField[" << a << "][" << b << "][" << c << "] = " << GaugeField[a][b][c] << "\n";
			}
		}
	}*/

	datalog.open(logfilepath, std::fstream::out | std::fstream::app);
	datalog << "ColdStart\n\n";
	datalog.close();
}

//-----

void HotStart(std::vector<std::vector<std::vector<double>>>& GaugeField)
{	
	/*GaugeField.resize(PathSize);

	#ifdef FIXED_SEED
	std::mt19937 generator_h(1);
	#else
	std::mt19937 generator_h(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_hotstart(-1.0,1.0);


	auto gen = [&distribution_hotstart, &generator_h](){return distribution_hotstart(generator_h);};
	std::generate(GaugeField.begin(), GaugeField.end(), gen);

	for (int a = 0; a < PathSize; ++a)
	{
		cout << "GaugeField = " << GaugeField[a] << "\t (HotStart)\n";
	}
	cout << "Size of Vector = " << GaugeField.size();*/
	
	//GaugeField.reserve(LatticeSize);
	//GaugeField.clear();

	#ifdef FIXED_SEED
	std::mt19937 generator_h(1);
	#else
	std::mt19937 generator_h(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_hotstart(0,2*M_PI);

	//Creates vector containing spins

	#pragma omp parallel for //private(b)
	for (int a = 0; a < N_s; ++a)
	{
		std::vector<std::vector<double>> GaugeFieldRow;
		//GaugeFieldRow.reserve(N_t);
		for (int b = 0; b < N_t; ++b)
		{
			std::vector<double> GaugeFieldDirection;
			GaugeFieldDirection.reserve(2);
			for (int c = 0; c < 2; ++c)
			{
				GaugeFieldDirection.push_back(distribution_hotstart(generator_h));
			}
			GaugeFieldRow.push_back(GaugeFieldDirection);
		}
		GaugeField.push_back(GaugeFieldRow);
	}

	/*for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			for (int c = 0; c < 2; ++c)
			{
				cout << "GaugeField[" << a << "][" << b << "][" << c << "] = " << GaugeField[a][b][c] << "\n";
			}
		}
	}*/

	datalog.open(logfilepath, std::fstream::out | std::fstream::app);
	datalog << "HotStart\n\n";
	datalog.close();
}

//-----
//Creates a random gauge transformation

void CreateGaugeTransform(std::vector<std::vector<double>>& Transformation)
{
	Transformation.clear();

	#ifdef FIXED_SEED
	std::mt19937 generator_g(2);
	#else
	std::mt19937 generator_g(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_gaugetransformation(0,2*M_PI);

	for (int a = 0; a < N_s; ++a)
	{
		std::vector<double> TransformationRow;
		TransformationRow.reserve(N_t);
		for (int b = 0; b < N_t; ++b)
		{
			TransformationRow.push_back(distribution_gaugetransformation(generator_g));
		}
		Transformation.push_back(TransformationRow);
	}

	// for (int a = 0; a < N_s; ++a)
	// {
	// 	for (int b = 0; b < N_t; ++b)
	// 	{
	// 		cout << "GaugeTransformation[" << a << "][" << b << "] = " << Transformation[a][b] << "\n";
	// 	}
	// }
}

//-----
//Creates a temporal gauge transformation (does not create the gauge transformation vector!)

void CreateTemporalGauge(const std::vector<std::vector<std::vector<double>>>& GaugeField, std::vector<std::vector<double>>& Transformation)
{
	for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			Transformation[a][b] = 0;
			for (int t = 0; t < b; ++t)
			{
				Transformation[a][b] += GaugeField[a][t][1];
			}
		}
	}
}

//-----
//Applies a gauge transformation to the fields

void GaugeTransform(std::vector<std::vector<std::vector<double>>>& GaugeField, const std::vector<std::vector<double>>& Transformation)
{
	cout << "Gauge transforming fields...\n";
	for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			GaugeField[a][b][0] += Transformation[a][b] - Transformation[(a + 1)%N_s][b];
			GaugeField[a][b][1] += Transformation[a][b] - Transformation[a][(b + 1)%N_t];
			/*for (int c = 0; c < 2 ; ++c)
			{
				GaugeField[a][b][c] += Transformation[a][b] - Transformation[a][];
			}*/
		}
	}
	cout << "\nNew fields:\n";
	for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			for (int c = 0; c < 2; ++c)
			{
				cout << "GaugeField[" << a << "][" << b << "][" << c << "] = " << GaugeField[a][b][c] << "\n";
			}
		}
	}
}

//-----
//Calculates the plaquette in two dimensions
	
inline std::complex<double> Plaquette(const std::vector<std::vector<std::vector<double>>>& GaugeField, int x, int y)
{
	return std::exp( i * (GaugeField[x][y][0] + GaugeField[(x+1)%N_s][y][1] - GaugeField[x][(y+1)%N_t][0] - GaugeField[x][y][1]));
}

//-----
//Calculates the daggered staple in two dimensions

inline std::complex<double> Staple(const std::vector<std::vector<std::vector<double>>>& GaugeField, int x, int y, int l)
{
	staple_1 = std::exp( i * (-GaugeField[x][y][1-l] - GaugeField[(x+l)%N_s][(y+1-l)%N_t][l] + GaugeField[(x+1-l)%N_s][(y+l)%N_t][1-l]));
	staple_2 = std::exp( i * (GaugeField[(x-l+N_s)%N_s][(y+l-1+N_s)%N_t][1-l] - GaugeField[(x-l+N_s)%N_s][(y+l-1+N_t)%N_t][l] - GaugeField[(x+1-2*l+N_s)%N_s][(y+2*l-1+N_t)%N_t][1-l]));
	staple_sum = staple_1 + staple_2;
	return staple_sum;
}

//-----
//Calculates the Topological Charge in two dimensions

inline double TopologicalCharge(const std::vector<std::vector<std::vector<double>>>& GaugeField)
{
	TopCharge = 0.0;
	for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			TopCharge += std::fmod(std::fmod(GaugeField[a][b][0] + GaugeField[(a + 1)%N_s][b][1] - GaugeField[a][(b + 1)%N_t][0] - GaugeField[a][b][1], 2*M_PI) + 3*M_PI, 2*M_PI) - M_PI;
		}
	}
	TopCharge *= TopNormalization; //* std::fmod(TopCharge, 2 * M_PI);
	return TopCharge;
}

//-----
//Calculates the Wilson action

double WilsonAction(const std::vector<std::vector<std::vector<double>>>& GaugeField)
{
	double Action = 0.0;
	for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			Action += 1 - std::real(Plaquette(GaugeField, a, b));
		}
	}
	Action *= (beta * LatticeSizeInverse);
	return Action;
}

//-----
//Resets the phases of the fields to be in the interval (0, 2*pi)

inline void ResetPhase(std::vector<std::vector<std::vector<double>>>& GaugeField)
{
	for (int a = 0; a < N_s; ++a)
	{
		for (int b = 0; b < N_t; ++b)
		{
			for (int c = 0; c < 2; ++c)
			{
				GaugeField[a][b][c] = std::fmod(std::fmod(GaugeField[a][b][c], 2*M_PI) + 3*M_PI, 2*M_PI) - M_PI;
			}
		}
	}
}

//-----

void MetropolisAlgorithm(std::vector<std::vector<std::vector<double>>>& GaugeField)
{
	double Suggestion;
	double DeltaAction;

	std::complex<double> PlaquetteObservable;

	//suggestion for new phase

	#ifdef FIXED_SEED
	std::mt19937 generator_suggestion(3);
	#else
	std::mt19937 generator_suggestion(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_suggestion(-0.1, 0.1);
	//std::normal_distribution<double> distribution_suggestion(0.0, 0.5);

	//probability

	#ifdef FIXED_SEED
	std::mt19937 generator_prob(4);
	#else
	std::mt19937 generator_prob(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_prob(0.0, 1.0);

	//coordinates

	// std::mt19937 generators(randomdevice());
	// std::mt19937 generatort(randomdevice());
	// std::mt19937 generatorl(randomdevice());


	// std::uniform_int_distribution<int> distribution_coord_s(0, N_s - 1);
	// std::uniform_int_distribution<int> distribution_coord_t(0, N_t - 1);
	// std::uniform_int_distribution<int> distribution_link(0, 1);

	// int s;
	// int t;
	// int l;

	//Thermalization

	while (n_count < 10000)
	{
		for (int a = 0; a < N_s; ++a)
		{
			for (int b = 0; b < N_t; ++b)
			{
				for (int c = 0; c < 2; ++c)
				{
					Suggestion = GaugeField[a][b][c] + distribution_suggestion(generator_suggestion);
					DeltaAction = beta * std::real((std::exp(i * GaugeField[a][b][c]) - std::exp(i * Suggestion)) * Staple(GaugeField, a, b, c));
					double p = std::exp(-DeltaAction);
					double q = distribution_prob(generator_prob);
					if (q < p)
					{
						GaugeField[a][b][c] = Suggestion;
						//cout << "Suggestion accepted \n";
					}
					/*else
					{
						cout << "Suggestion rejected \n";
					}*/
				}
			}
		}
		n_count++;
	}
	n_count = 0;

	datalog.open(logfilepath, std::fstream::out | std::fstream::app);
	//positionlog.open(positionfilepath, std::fstream::out | std::fstream::app);
	//correlationlog.open(correlationfilepath, std::fstream::out | std::fstream::app);

	while (n_count < n_run)
	{
		for (int a = 0; a < N_s; ++a)
		{
			for (int b = 0; b < N_t; ++b)
			{
				for (int c = 0; c < 2; ++c)
				{
					Suggestion = GaugeField[a][b][c] + distribution_suggestion(generator_suggestion);
					DeltaAction = beta * std::real((std::exp(i * GaugeField[a][b][c]) - std::exp(i * Suggestion)) * Staple(GaugeField, a, b, c));
					double p = std::exp(-DeltaAction);
					double q = distribution_prob(generator_prob);
					if (q < p)
					{
						GaugeField[a][b][c] = Suggestion;
						//cout << "Suggestion accepted \n\n";
					}
					/*else
					{
						cout << "Suggestion rejected \n\n";
					}*/
				}
			}
		}

		// for (int a = 0; a < 144; ++a)
		// {
		// 	s = distribution_coord_s(generators);
		// 	t = distribution_coord_t(generatort);
		// 	l = distribution_link(generatorl);

		// 	Suggestion = GaugeField[s][t][l] + distribution_suggestion(generator_suggestion);
		// 	DeltaAction = beta * std::real((std::exp(i * GaugeField[s][t][l]) - std::exp(i * Suggestion)) * Staple(GaugeField, s, t));
		// 	//double p = std::min(exp(-DeltaAction), 1.0);
		// 	double p = std::exp(-DeltaAction);
		// 	double q = distribution_prob(generator_prob);
		// 	if (q < p)
		// 	{
		// 		GaugeField[s][t][l] = Suggestion;
		// 		//cout << "Suggestion accepted \n";
		// 	}
		// 	else
		// 	{
		// 		//cout << "Suggestion rejected \n";
		// 	}
		// }

		if (n_count%expectation_period == 0)
		{	
			PlaquetteObservable = {0.0, 0.0};

			for (int a = 0; a < N_s; ++a)
			{
				for (int b = 0; b < N_t; ++b)
				{
					PlaquetteObservable += Plaquette(GaugeField, a, b);
				}
			}

			PlaquetteObservable *= LatticeSizeInverse;

			datalog << "Step " << n_count << "\n";
			datalog << "Action: " << WilsonAction(GaugeField) << "\n";
			datalog << "TopCharge: " << TopologicalCharge(GaugeField) << "\n";
			datalog << "Plaquette_Re: " << std::real(PlaquetteObservable) << "\n";
			datalog << "Plaquette_Im: " << std::imag(PlaquetteObservable) << "\n" << endl;
		}
		if (n_count%10000 == 0)
		{
			ResetPhase(GaugeField);
		}
		n_count++;
	}
	datalog.close();
	//positionlog.close();
	//correlationlog.close();
}

//-----

int main()
{
	Configuration();
	CreateFiles();
	HotStart(GaugeField);
	//ColdStart(GaugeField);


	CreateGaugeTransform(GaugeTransformation);
	CreateTemporalGauge(GaugeField, GaugeTransformation);
	GaugeTransform(GaugeField, GaugeTransformation);


	// cout << "Action: " << WilsonAction(GaugeField) << "\n";
	// cout << "\nPlaquette: " << Plaquette(GaugeField, 2, 2) << "\n";


	MetropolisAlgorithm(GaugeField);
}