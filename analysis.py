import math
import random
import sys
import os
import shutil
import numpy as np
from scipy import special
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

#-----
#Current version
current_version = "U(1)_Gauge_Theory_1.1"

#Thermalization phase (discard all expectation values before therm)
therm = 10

#Random seed
seed = 12345
random.seed(seed)
np.random.seed(seed)

#Number or resamples
n_resample = 250

#Size of bootstrap blocks
blocksize = 4

#Colormap for plots
#plt.set_cmap("jet")			#Red and blue
#plt.set_cmap("viridis")		#Yellow and purple
plt.set_cmap("cividis")			#Yellow and blue

#-----
#Get user input on what to do

print("\nData analysis program for " + current_version + "\n")
#user_choice = input("Start new analysis (1), or load partition functions from existing values (2): ")
#print(user_choice)

#if user_choice != "1" and user_choice != "2":
#	print("Invalid input, exiting program.")
#	sys.exit()

#-----
#Go through all subdirectories

main_dir = os.getcwd()
directories = [os.path.abspath(x[0]) for x in os.walk(main_dir) if "log.txt" in x[2]]
directories.sort()
n_subdirectories = len(directories)

#-----
#Get values for n_run and expectation_period

n_run_list = []
expectation_period_list = []

for a in directories:
	if os.path.isfile(os.path.join(a, "log.txt")) == True:
		with open(os.path.join(a, "log.txt")) as logfile:
			first_line = logfile.readline().strip()
			if first_line == current_version:
				print("Compatible file found in " + a + "\n")
				n_run = None
				expectation_period = None
				for line in logfile:
					if not (len(line.strip()) == 0):
						if line.split()[0] == "N_s":
							N_s = int (line.split()[2])
						if line.split()[0] == "N_t":
							N_t = int (line.split()[2])
						if line.split()[0] == "n_run":
							n_run = int(line.split()[2])
							n_run_list.append(n_run)
						if line.split()[0] == "expectation_period":
							expectation_period = int(line.split()[2])
							expectation_period_list.append(expectation_period)
						if (not n_run is None) and (not expectation_period is None):
							break
			else:
				print("Incompatible version in " + a)
	else:
		print("Could not find file \"log.txt\" in the current directory " + a)

#-----
#Exit program if differing values for n_run or expectation_period are found

abort = False

if len(set(n_run_list)) != 1:
	print("Differing values for n_run found")
	abort = True
if len(set(expectation_period_list)) != 1:
	print("Differing values for expectation_period found")
	abort = True

if abort:
	print("Exiting program")
	sys.exit()
print("\n" + "-"*shutil.get_terminal_size().columns + "\n")

#-----
#Setup arrays for all observables

n_expectation_values = int(n_run/expectation_period)

action_raw = np.zeros(n_expectation_values)
topcharge_raw = np.zeros(n_expectation_values)
plaquette_re_raw = np.zeros(n_expectation_values)
plaquette_im_raw = np.zeros(n_expectation_values)

#-----
#Bootstrap
#Generates bootstrap_array which can be used as weight

bootstrap_array = np.zeros((n_resample, n_expectation_values))

n_blocks = n_expectation_values // blocksize
for i in range(0, n_resample):
	for j in range(n_blocks):
		bs_index = np.random.randint(0, n_expectation_values)
		for k in range (blocksize):
			bootstrap_array[i, (bs_index + k) % n_expectation_values] += 1

print(bootstrap_array)

#function for bootstrapping observable
#input consists of raw data of observable and the already calculated expectation values of the observable
#works with observables of variable length (e.g. correlation functions)

def bootstrap(observable_raw, observable_expectation_value):
	try:
		observable_length = np.size(observable_raw, axis = 1)
	except:
		observable_length = 1
		print("Observable length along axis 1 not defined, using 1 instead.")
	observable_resampled = np.zeros((n_resample, observable_length))
	for i in range(0, n_resample):
		observable_resampled[i] = np.average(observable_raw, axis = 0, weights = bootstrap_array[i])
	observable_error = np.sqrt(np.var(observable_resampled, axis = 0))
	observable_final = np.array([observable_expectation_value, observable_error])
	return observable_final, observable_resampled

#-----
#Get all values for observables and plot first and final configurations

for a in directories:

	if os.path.isfile(os.path.join(a, "log.txt")) == True:
		with open(os.path.join(a, "log.txt")) as logfile:
			if first_line == current_version:
				print("Compatible file found in " + a)
				#for count_mc, line in enumerate(logfile):
				count_mc = -1
				for line in logfile:
					if not (len(line.strip()) == 0):
						if line.split()[0] == "Action:":
							action_raw[count_mc] = float(line.split()[1])
						if line.split()[0] == "TopCharge:":
							topcharge_raw[count_mc] = float(line.split()[1])
						if line.split()[0] == "Plaquette_Re:":
							plaquette_re_raw[count_mc] = float(line.split()[1])
						if line.split()[0] == "Plaquette_Im:":
							plaquette_im_raw[count_mc] = float(line.split()[1])
						if line.split()[0] == "Step":
							count_mc += 1
			else:
				print("Incompatible version in " + a)
	else:
		print("Could not find file \"log.txt\" in the current directory " + a)

#-----
#Calculate observables

	print("Average action:", np.mean(action_raw))
	print("Plaquette_Re:", np.mean(plaquette_re_raw))
	print("Plaquette_Im:", np.mean(plaquette_im_raw))

	action_final, action_resampled = bootstrap(action_raw, np.mean(action_raw))
	plaquette_re_final, plaquette_re_resampled = bootstrap(plaquette_re_raw, np.mean(plaquette_re_raw))
	plaquette_im_final, plaquette_im_resampled = bootstrap(plaquette_im_raw, np.mean(plaquette_im_raw))

	# position = np.loadtxt(os.path.join(a, "position.txt"), dtype = float, delimiter = ",")

	# correlation_raw = np.loadtxt(os.path.join(a, "correlation.txt"), dtype = float, delimiter = ",")
	# print("Shape", np.shape(correlation_raw))
	# correlation_function = np.zeros(Pathsize)
	# correlation_function = np.mean(correlation_raw, axis = 0)
	# Delta_tau = 1
	# np.seterr(invalid="ignore")
	# effective_energy_3pt = np.arccosh((np.roll(correlation_function, Delta_tau) + np.roll(correlation_function, -Delta_tau))/(2*correlation_function))
	# #= 1/Delta_tau * np.log(correlation_function/np.roll(correlation_function, -Delta_tau))

	# correlation_function_final, correlation_function_resampled = bootstrap(correlation_raw, correlation_function)

	# effective_energy_3pt_resampled = np.arccosh((np.roll(correlation_function_resampled, Delta_tau, axis = 1) + np.roll(correlation_function_resampled, -Delta_tau, axis = 1))/(2*correlation_function_resampled))
	# effective_energy_3pt_error = np.sqrt(np.var(effective_energy_3pt_resampled, axis = 0))
	# effective_energy_3pt_final = np.array([effective_energy_3pt, effective_energy_3pt_error])

#-----
#Output raw data as .txt or .csv

	np.savetxt(os.path.join(a, "action.txt"), action_final, delimiter = ",")
	np.savetxt(os.path.join(a, "topological_charge.txt"), topcharge_raw, delimiter = ",")
	np.savetxt(os.path.join(a, "plaquette_real.txt"), plaquette_re_final.T, delimiter = ",")
	np.savetxt(os.path.join(a, "plaquette_imaginary.txt"), plaquette_im_final.T, delimiter = ",")

#-----
#Generate plots

	#Plot histogram of action

	plt.hist(action_raw, bins = 100)
	plt.xlabel("Action")
	plt.title("Histogram (32x32 lattice, β = 12.8)")
	plt.savefig(os.path.join(a, "action_hist.pdf"))
	plt.close()	

	#Plot histogram of topological charge

	plt.hist(topcharge_raw, bins = 200)
	plt.xlabel("Topological Charge")
	plt.title("Histogram (32x32 lattice, β = 12.8)")
	plt.savefig(os.path.join(a, "topological_charge_hist.pdf"))
	plt.close()

	#Plot action

	plt.plot(action_raw[0:100])
	plt.xlabel("Configuration")
	plt.ylabel("Action")
	plt.title("Action (32x32 lattice, β = 12.8)")
	plt.savefig(os.path.join(a, "action.pdf"))
	plt.close()

	#Plot topological charge

	#plt.rc('grid', linestyle="--")

	plt.grid(True)
	fig = plt.figure() 
	ax = fig.gca()
	ax.set_xticks(np.arange(0,110,10))
	ax.set_xticks(np.arange(0,110,5), minor = True)
	ax.set_yticks(np.arange(-20,60,10))
	ax.set_yticks(np.arange(-20,60,2), minor = True)
	ax.grid(True)
	ax.grid(b = True, which = "major", linestyle = "--", linewidth = "0.5")
	ax.grid(b = True, which = "minor", linestyle = "--", linewidth = "0.25")
	plt.ylim(-20,60)
	plt.plot(topcharge_raw[0:100])
	plt.xlabel("Configuration")
	plt.ylabel("Topological charge")
	plt.title("Topological charge (32x32 lattice, β = 12.8)")
	plt.savefig(os.path.join(a, "topological_charge.pdf"))
	plt.close()

	# #Plot correlation function

	# plt.errorbar(np.arange(0, Pathsize), correlation_function_final[0], yerr = correlation_function_final[1], linestyle = "", marker = ".")
	# plt.yscale("log")
	# plt.xlabel("Distance")
	# #plt.ylabel("Correlation function")
	# plt.title("Correlation function")
	# plt.savefig(os.path.join(a, "correlation_function.pdf"))
	# plt.close()

	# #Plot effective energy

	# plt.errorbar(np.arange(0, Pathsize), effective_energy_3pt_final[0], yerr = effective_energy_3pt_final[1], linestyle="", marker = ".")
	# plt.ylim([0,1])
	# plt.savefig(os.path.join(a, "effective_energy_3pt.pdf"))
	# plt.close()
